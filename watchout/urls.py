# watchout
# watchout/urls.py

from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.views.i18n import javascript_catalog
from django.views.generic import TemplateView

admin.autodiscover()

JS_I18N_PACKAGES = (
    '',
    'dcl',
    'dacl',
    'watchout.apps.events',
)

urlpatterns = patterns('', )

# third part apps urls patterns
urlpatterns += patterns('',
    url(r'^redactor/', include('redactor.urls')),
)

# default django urls patterns
urlpatterns += patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^jsi18n/$', javascript_catalog, {'domain': 'djangojs', 'packages': JS_I18N_PACKAGES, }, name='jsi18n'),
)

# watchout urls patterns
urlpatterns += patterns('',
    url(r'^$', TemplateView.as_view(template_name="index.html"), name='index'),  # index
    url(r'^events/', include('watchout.apps.events.urls')),  # events
)

# debugging and development urls hooks
if settings.SERVE_STATIC:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT, 'show_indexes': True, }),
    )
