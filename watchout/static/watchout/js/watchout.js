"use strict";
// watchout
// watchout/static/watchout/js/watchout.js

var map,
    events,
    markers = [],
    infowindows = [],
    infoWindowTpl = _.template($('#id-info-window-tpl').html());

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 14,
        disableDefaultUI: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: [  // disable show local places and business on map
            {
                featureType: "poi",
                stylers: [
                    {
                        visibility: "off"
                    }
                ]
            }
        ]
    });

    $(window).on("eventsLoaded",function(){
        if (markers.length) {
                markers.forEach(function(el){
                    el.setMap(null);
                });
            markers = [];
        }
        if ($(".items-list__item").length){
            $(".items-list").empty();
        }
        events.forEach(function(event, i){
            if (event.name==="Garage 48 Lviv"){
                var marker = new google.maps.Marker({
                    position: {
                        lat: event.latitude,
                        lng: event.longitude
                    },
                    map: map,
                    icon: {
                        url: "http://s.developers.org.ua/img/events/garage48-logo-400x400.png",
                        scaledSize: new google.maps.Size(40, 40)
                    }
                });
            } else if(event.name==="Романтичний квест") {
                var marker = new google.maps.Marker({
                    position: {
                        lat: event.latitude,
                        lng: event.longitude
                    },
                    map: map,
                    icon: {
                        url: "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/Heart_coraz%C3%B3n.svg/2000px-Heart_coraz%C3%B3n.svg.png",
                        scaledSize: new google.maps.Size(40, 40)
                    }
                });
            } else if(event.name==='Вечірка у Ринку Зброї "Під Арсеналом"') {
                var marker = new google.maps.Marker({
                    position: {
                        lat: event.latitude,
                        lng: event.longitude
                    },
                    map: map,
                    icon: {
                        url: "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Party_icon.svg/317px-Party_icon.svg.png",
                        scaledSize: new google.maps.Size(40, 40)
                    }
                });
            } else {
                var marker = new google.maps.Marker({
                    position: {
                        lat: event.latitude,
                        lng: event.longitude
                    },
                    map: map,
                    // icon: {
                    //     url: sprintf("/static/watchout/img/icons/%(icon)s", {icon: event.icon || 'unknown.png'}),
                    //     scaledSize: new google.maps.Size(50, 50)
                    // }
                });
            }
            var infowindow = new google.maps.InfoWindow({
                content: infoWindowTpl({event: event})
            });

            infowindows.push(infowindow);
            marker.addListener('click', function() {
                infowindows.forEach(function(el){
                    el.close(map, marker);
                });
                infowindow.open(map, marker);
                // map.setCenter(marker.getPosition());
            });
            markers.push(marker);
            var diff = ~~((new Date(event.start) - new Date())/36e5) + " hours till event";
            if (parseInt(diff,10)<=0) diff = "Event has already started";
            if (i<5){
                var el = $(".items-list").append("<div class='items-list__item' data-markerid="+i+"><div class='items-list__item-name'>"+event.name+"</div><div class='items-list__item-time'>"+diff+", it was scheduled for: "+event.start+"</div><div class='items-list__item-location'>"+event.address+"</div></div>");
            }
            $(".items-list__item").on("click",function(){
                map.setCenter(markers[i].getPosition());
            });
        });
    });

    $(document).ready(function() {
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy'
        });
        $('.datepicker').datepicker('setDate', new Date());
        var currentLocation;
        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var initialLocation = new google.maps.LatLng(
                    position.coords.latitude,
                    position.coords.longitude
                );
                map.setCenter(initialLocation);
                updateEvents();
            },function() {
                var initialLocation = {lat: 49.839683, lng: 24.029717};
                map.setCenter(initialLocation);
                updateEvents();
            });
        }
        var updateEvents = function() {
            var query = $("#search_word").val();
            var date = $("#date").val();
            var currDate = new Date();
            // var fromTime = $("#time_from").val();
            function addZero(i) {
                return i < 10 ? "0" + i : i;
            }
            function addHoursFromNow(hours) {
                return new Date(new Date().getTime() + parseInt(hours,10)*3600000);
            }
            var fromTime = addZero(currDate.getHours())+":"+addZero(currDate.getMinutes());
            var toTime = addHoursFromNow($("#time_to").val());
                toTime = addZero(toTime.getHours())+":"+addZero(toTime.getMinutes());

            console.log(date, fromTime, toTime);

            try {
                var lat0 = map.getBounds().getNorthEast().lat(),
                    lng0 = map.getBounds().getNorthEast().lng(),
                    lat1 = map.getBounds().getSouthWest().lat(),
                    lng1 = map.getBounds().getSouthWest().lng(),
                    currentLocation = [lat0, lng0, lat1, lng1].join(',');
            } catch (err) {
                var currentLocation = '';
            }

            query = query ? sprintf("name__icontains=%(query)s", {query: query}) : null;
            currentLocation = currentLocation ? sprintf("current_location=%(currentLocation)s", {currentLocation: currentLocation}) : null;
            $.getJSON(sprintf("/events/api/v1/event/?%(query)s&%(currentLocation)s", {query: query, currentLocation: currentLocation}), function(data) {
                events = data.objects;
                $(window).trigger("eventsLoaded");
            });
        };
        updateEvents();
        $("#submit").on("click",updateEvents);
        $("#search_word").on("keyup",updateEvents);
        google.maps.event.addListener(map, 'idle', function() {
            updateEvents()
        });
        // animations
        $(window).on("show-interface",function(){
            setTimeout(function(){
                $(".tutorial").removeClass("tutorial-show");
                setTimeout(function(){
                    $(".tutorial").hide();
                },100);
                $("body").addClass("show-interface");
            },300);
            setTimeout(function(){
                google.maps.event.trigger(map, "resize");
            },1000);
        });
        if(typeof(Storage) !== "undefined") {
            // remove comments to show infowindow only once
            // if (typeof localStorage.seen===undefined) {
                localStorage.seen = true;
                setTutorials();
            // } else {
            //     $(window).trigger("show-interface");
            // }
        } else {
            setTutorials();
        }
        function setTutorials(){
            setTimeout(function(){
                $(".tutorial").addClass("tutorial-show");
                var t = 500,
                    nextTime = 3000,
                    hideAllTime = $(".tutorial h1").length*nextTime + 500;
                $(".tutorial h1").each(function(){
                    var that = $(this);
                    setTimeout(function(){
                        $(".tutorial h1").removeClass("fadeIn");
                        that.addClass("fadeIn");
                    },t);
                    t+=nextTime;
                });
                setTimeout(function(){
                    $(window).trigger("show-interface");
                },hideAllTime);
            },500);
            $(".tutorial-close, .tutorial-bg").on("click",function(){
                $(".tutorial").removeClass("tutorial-show");
                setTimeout(function(){
                    $(".tutorial").hide();
                },500);
                $(window).trigger("show-interface");
            });
        }
    });
}
