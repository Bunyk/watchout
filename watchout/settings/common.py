# watchout
# watchout/settings/common.py

import sys
import os
from fnmatch import fnmatch


class GlobList(list):
    """
    Allows to include globs of IP addresses in INTERNAL_IPS.
    """

    def __contains__(self, key):
        for elt in self:
            if fnmatch(key, elt):
                return True
        return False

PROJECT_NAME = u'watchout'

PROJECT_PATH = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))).replace('\\', '/')

sys.path.insert(0, PROJECT_PATH)

ADMINS = ()
MANAGERS = ADMINS

TIME_ZONE = u'UTC'
LANGUAGE_CODE = u'en'
LOCALE_PATHS = (
    os.path.join(PROJECT_PATH, PROJECT_NAME, u'locale').replace('\\', '/'),
    os.path.join(PROJECT_PATH, PROJECT_NAME, u'apps/events/locale').replace('\\', '/'),
)

SITE_ID = 1
USE_I18N = True
USE_L10N = True

USE_TZ = True

MEDIA_ROOT = os.path.join(PROJECT_PATH, u'media').replace('\\', '/')
STATIC_ROOT = os.path.join(PROJECT_PATH, u'static').replace('\\', '/')
STATICFILES_DIRS = (
    os.path.join(PROJECT_PATH, PROJECT_NAME, u'static').replace('\\', '/'),
)

SECRET_KEY = '-+2*9j7$_m)0jt%!y)k=onl6-3$-i90_1!u#j(b2j8#l18cx#e'

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.i18n',
    'django.core.context_processors.request',
    'django.core.context_processors.csrf',
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    # third part context processors
    # watchout context processor
)

MIDDLEWARE_CLASSES = (
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # third part middlewares
    # watchout middlewares
)

TEMPLATE_DIRS = (
    os.path.join(PROJECT_PATH, PROJECT_NAME, u'templates').replace('\\', '/'),
)

ROOT_URLCONF = 'watchout.urls'
APPEND_SLASH = True

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.eggs.Loader',
)

INSTALLED_APPS = (
    'flat',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.staticfiles',
    # third part django apps
    'redactor',
    'formtools',
    'tastypie',
    'location_field',
    'sitemetrics',
    # watchout apps
    'watchout.apps.events',
)

# django mssages settings
MESSAGE_STORAGE = 'django.contrib.messages.storage.fallback.FallbackStorage'

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

# api credentials

# auth settings
AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.ModelBackend',
]
LOGIN_REDIRECT_URL = '/'

# redactor settings
REDACTOR_OPTIONS = {'lang': 'en', "buttonSource": True, }
REDACTOR_UPLOAD = 'uploads/'

# api
CURRENT_API_VERSION = u'v1'
TASTYPIE_DEFAULT_FORMATS = ['json', ]
CORS_ORIGIN_ALLOW_ALL = True
