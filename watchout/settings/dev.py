# watchout
# watchout/settings/dev.py

from watchout.settings.common import *

ENVIRONMENT = ''

DEBUG = True
TEMPLATE_DEBUG = DEBUG

SERVE_STATIC = True

# for old django and django reusable applications config fix
DATABASE_ENGINE = 'sqlite3'
DATABASE_NAME = os.path.join(PROJECT_PATH, 'data/db/%s.sqlite3' % PROJECT_NAME).replace('\\', '/')
DATABASE_USER = ''
DATABASE_PASSWORD = ''
DATABASE_HOST = ''
DATABASE_PORT = ''

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.%s' % DATABASE_ENGINE,
        'NAME': DATABASE_NAME,
        'USER': DATABASE_USER,
        'PASSWORD': DATABASE_PASSWORD,
        'HOST': DATABASE_HOST,
        'PORT': DATABASE_PORT,
    },
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
        'TIMEOUT': 600,
        'KEY_PREFIX': PROJECT_NAME,
        'OPTIONS': {
            'MAX_ENTRIES': 9999,
        },
    },
}

# django e-mail settings
EMAIL_HOST = '127.0.0.1'
EMAIL_PORT = 1025
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_USE_TLS = False
DEFAULT_FROM_EMAIL = 'noreply@watchout.local'

TEMPLATE_CONTEXT_PROCESSORS += (
    'django.core.context_processors.debug',
)

INTERNAL_IPS = GlobList([
    '127.0.0.1',
])

# storages settings
STATIC_URL = '/static/'
MEDIA_URL = '/media/'

WSGI_APPLICATION = 'watchout.wsgi.dev.application'
