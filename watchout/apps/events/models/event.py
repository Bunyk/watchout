# watchout
# watchout/apps/events/models/event.py

from django.db import models
from django.utils.translation import ugettext_lazy as _

from location_field.models.plain import PlainLocationField
from redactor.fields import RedactorField

from watchout.apps.events.utils import location_from_string
from watchout.apps.events.managers import EventManager

__all__ = ['Event', ]


class Event(models.Model):
    """
    Event model.
    """

    name = models.CharField(verbose_name=_(u'name'), db_index=True, max_length=256)
    description = RedactorField(verbose_name=_(u'description'), db_index=True, null=True, blank=True)
    start = models.DateTimeField(verbose_name=_(u'event start'), db_index=True)
    end = models.DateTimeField(verbose_name=_(u'event end'), db_index=True)
    address = models.CharField(verbose_name=_(u'address'), db_index=True, max_length=256, default=u'')
    location = PlainLocationField(based_fields=[address], zoom=7)
    category = models.ForeignKey('events.Category', verbose_name=_(u'category'))

    objects = EventManager()

    def __unicode__(self):

        return u"%s: %s - %s" % (self.name, self.start, self.end)

    class Meta:
        app_label = 'events'
        verbose_name = _(u'event')
        verbose_name_plural = _(u'events')
        ordering = ['start', ]
        unique_together = ['name', 'start', ]

    @property
    def latitude(self):
        """
        Return latitude from location.
        """

        latitude, longitude = location_from_string(self.location)

        return latitude

    @property
    def longitude(self):
        """
        Return longitude from location.
        """

        latitude, longitude = location_from_string(self.location)

        return longitude
