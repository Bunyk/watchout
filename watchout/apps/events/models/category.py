# watchout
# watchout/apps/events/models/category.py

from django.db import models
from django.utils.translation import ugettext_lazy as _

__all__ = ['Category', ]


class Category(models.Model):
    """
    Category model.
    """

    name = models.CharField(verbose_name=_(u'name'), db_index=True, max_length=256, unique=True)

    def __unicode__(self):

        return self.name

    class Meta:
        app_label = 'events'
        verbose_name = _(u'category')
        verbose_name_plural = _(u'categories')
        ordering = ['name', ]
