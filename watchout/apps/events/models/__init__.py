# watchout
# watchout/apps/events/models/__init__.py

from watchout.apps.events.models.event import Event
from watchout.apps.events.models.category import Category

__all__ = ['Event', 'Category', ]
