# watchout
# watchout/apps/events/__init__.py

__all__ = [
    'models',
    'admin',
    'urls',
    'api',
    'default_app_config',
    'managers',
    'parsers',
]

default_app_config = 'watchout.apps.events.apps.Config'
