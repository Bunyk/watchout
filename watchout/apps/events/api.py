# watchout
# watchout/apps/events/api.py

from django.conf import settings

from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie import fields

from watchout.apps.events.models import Event, Category

__all__ = ['EventResource', 'CategoryResource', ]


class CategoryResource(ModelResource):
    """
    Category api resource.
    """

    class Meta:

        queryset = Category.objects.all()
        resource_name = 'category'
        filtering = {
            'id': ALL,
            'name': ALL,
        }
        api_name = settings.CURRENT_API_VERSION
        object_class = Category


class EventResource(ModelResource):
    """
    Event api resource.
    """

    category = fields.ForeignKey(CategoryResource, 'category')

    class Meta:

        queryset = Event.objects.active()
        resource_name = 'event'
        filtering = {
            'id': ALL,
            'name': ALL,
            'description': ALL,
            'start': ALL,
            'end': ALL,
            'address': ALL,
            'location': ALL,
            'category': ALL_WITH_RELATIONS,
        }
        api_name = settings.CURRENT_API_VERSION
        object_class = Event

    def dehydrate(self, bundle):

        bundle.data['latitude'] = bundle.obj.latitude
        bundle.data['longitude'] = bundle.obj.longitude

        return bundle

    def filter_by_radius_points(self, request, current_location):
        """
        Return events filtered by radius.
        """
        lat0, lng0, lat1, lng1 = map(float, current_location.split(','))

        latitude__min = min(lat0, lat1)
        latitude__max = max(lat0, lat1)
        longitude__min = min(lng0, lng1)
        longitude__max = max(lng0, lng1)
        
        events_ids = []
        for event in super(EventResource, self).get_object_list(request).all():
            if all([event.latitude >= latitude__min, event.latitude <= latitude__max, event.longitude >= longitude__min, event.longitude <= longitude__max, ]):
                events_ids.append(event.pk)

        return super(EventResource, self).get_object_list(request).filter(pk__in=events_ids)

    def get_object_list(self, request):
        current_location = request.GET.get('current_location', None)
        if current_location:
            return self.filter_by_radius_points(request, current_location)
        else:
            return super(EventResource, self).get_object_list(request)
