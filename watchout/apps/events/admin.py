# watchout
# watchout/apps/events/admin.py

from django.contrib import admin

from watchout.apps.events.models import Event, Category

__all__ = ['EventAdmin', 'CategoryAdmin', ]


class EventAdmin(admin.ModelAdmin):
    """
    Customize Event model for admin area.
    """

    list_display = ['name', 'start', 'end', 'category', 'description', 'address', ]
    list_filter = ['category', ]


class CategoryAdmin(admin.ModelAdmin):
    """
    Customize Category model for admin area.
    """

    list_display = ['name', ]


# registering admin custom classes
admin.site.register(Event, EventAdmin)
admin.site.register(Category, CategoryAdmin)
