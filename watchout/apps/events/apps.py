# watchout
# watchout/apps/events/apps.py

from django.apps import AppConfig

__all__ = ['Config', ]


class Config(AppConfig):
    """
    Events app config.
    """

    name = u'watchout.apps.events'
    verbose_name = u'Watchout Core Application'
