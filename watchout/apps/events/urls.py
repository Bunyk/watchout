# watchout
# watchout/apps/events/urls.py

from django.conf.urls import patterns, url, include
from django.conf import settings

from tastypie.api import Api

from watchout.apps.events.api import EventResource, CategoryResource

api = Api(api_name=settings.CURRENT_API_VERSION)
api.register(EventResource())
api.register(CategoryResource())

# events urls
urlpatterns = patterns('',
    url(r'^api/', include(api.urls)),
)
