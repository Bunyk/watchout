# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import location_field.models.plain


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0002_auto_20151107_0142'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='address',
            field=models.CharField(default='', max_length=256, verbose_name='address', db_index=True),
        ),
        migrations.AddField(
            model_name='event',
            name='location',
            field=location_field.models.plain.PlainLocationField(default='', max_length=63),
            preserve_default=False,
        ),
    ]
