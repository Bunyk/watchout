# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import redactor.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=256, verbose_name='name', db_index=True)),
                ('description', redactor.fields.RedactorField(null=True, verbose_name='description', db_index=True)),
                ('start', models.DateTimeField(verbose_name='event start', db_index=True)),
                ('end', models.DateTimeField(verbose_name='event end', db_index=True)),
            ],
            options={
                'ordering': ['start'],
                'verbose_name': 'event',
                'verbose_name_plural': 'events',
            },
        ),
        migrations.AlterUniqueTogether(
            name='event',
            unique_together=set([('name', 'start')]),
        ),
    ]
