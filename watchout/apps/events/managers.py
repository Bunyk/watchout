# watchout
# watchout/apps/events/managers.py

from datetime import datetime

from django.db import models

__all__ = ['EventManager', ]


class EventManager(models.Manager):
    """
    Event manager.
    """

    def active(self):
        """
        Return events with start/end datetime greater than now datetime.
        """

        now = datetime.now()

        return self.filter(models.Q(start__gte=now) | models.Q(end__gte=now))
