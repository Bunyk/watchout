# coding=utf-8

from lxml import etree
from datetime import datetime, timedelta

from django.utils import timezone
from django.db.utils import IntegrityError

from watchout.apps.events.models import Event, Category

def create_films():
    cinema = Category.objects.get_or_create(name='cinema')[0]

    duplicate_count = 0
    for film, coords, date_str, address in yield_films():
        print date_str, film, coords
        date = datetime.strptime(date_str, '%Y-%m-%d %H:%M:%S')
        timezone.make_aware(date, timezone.get_current_timezone())
        try:
            Event.objects.create(
                name=film,
                start=date - timedelta(minutes=15),
                end=date + timedelta(minutes=15),
                address=address,
                location=coords,
                description=u'Кіносеанс',
                category=cinema
            )
        except IntegrityError as e:
            duplicate_count += 1
    print 'Duplicates found:', duplicate_count

THEATERS = [
{
    'address': u'вул. Стрийська, 30, Львівська область',
    'coords': '49.772783,24.0085163',
    'url': 'http://planetakino.ua/lvov/ua/showtimes/xml/'
}, {
    'address': u'вулиця Під Дубом, 7, Львів',
    'coords': '49.8498085,24.0233361',
    'url': 'http://planetakino.ua/lvov2/ua/showtimes/xml/',
}, {
    'address': u'Московський просп. 34В, Київ, Київська область, 04655',
    'coords': '50.4860913,30.5195017',
    'url': 'http://planetakino.ua/ua/showtimes/xml/',
}]

def parse_theater(url):
    ''' Yield title date pairs '''
    xml = etree.parse(url)

    titles = {}
    for movie in xml.xpath('//movies/movie'):
        movie_id = movie.attrib['id']
        title = movie.xpath('title/text()')[0]
        titles[movie_id] = title

    for show in xml.xpath('//showtimes/day/show'):
        date = show.attrib['full-date'] 
        movie_id = show.attrib['movie-id']

        yield titles[movie_id], date

def yield_films():
    for theater in THEATERS:
        for title, date in parse_theater(theater['url']):
            yield title, theater['coords'], date, theater['address']
