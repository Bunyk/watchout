# watchout
# watchout/apps/events/parsers/otot.py

import re
from datetime import timedelta, date, datetime
from time import strptime, mktime

from lxml import html
import requests

from watchout.apps.events.parsers.base import BaseParser
from watchout.apps.events.models import Category, Event

__all__ = ['OtOtParser', ]


class OtOtParser(BaseParser):
    """
    http://www.ot-ot.lviv.ua parser.
    """

    def __init__(self, category):

        self.category = Category.objects.get(pk=category)

        super(OtOtParser, self).__init__()

    def get_pages(self):

        return ['http://www.ot-ot.lviv.ua/uk/home/today/?eventdate={0}'.format(single_date.strftime("%Y%m%d")) for single_date in daterange(start_date, end_date)]

    def parse_page(self, url):
        """
        Parse page data.
        """

        page = requests.get(url)
        tree = html.fromstring(page.content)
        abc = tree.xpath('//div[@class="title"]/a/@href')

        for elem in abc:
            try:
                event_page = requests.get(elem)
                tree = html.fromstring(event_page.content)
                event_name = unicode(tree.xpath('//h1[@itemprop="name"]/text()')[0])
                start_date = tree.xpath('//*[@id="event-figure"]/div[2]/b/meta[1]/@content')[0]
                end_date = tree.xpath('//*[@id="event-figure"]/div[2]/b/meta[2]/@content')[0]
                event_location = tree.xpath('//span[@itemprop="offers"]/b/a/@href')
                location_page = requests.get(event_location[0])
                location_finder = re.compile(r'centerLatLng:.*?\'(.*?)\'')
                search_results = location_finder.search(location_page.content)
                event_location = search_results.groups()[0]
                print start_date, end_date
                print event_name, strptime(start_date, '%Y-%m-%dT%H:%M'), strptime(end_date, '%Y-%m-%dT%H:%M')
                o = Event.objects.create(
                    category=self.category,
                    name=event_name,
                    location=event_location,
                    start=datetime.fromtimestamp(mktime(strptime(start_date, '%Y-%m-%dT%H:%M'))),
                    end=datetime.fromtimestamp(mktime(strptime(end_date, '%Y-%m-%dT%H:%M'))),
                    address=u' '
                )
            except Exception, error:
                pass


start_date = date(2015, 11, 8)
end_date = date(2015, 12, 1)


def daterange(start_date, end_date):

    for n in range(int((end_date - start_date).days)):
        yield start_date + timedelta(n)
