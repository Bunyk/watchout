# watchout
# watchout/apps/events/parsers/base.py

import sys
import threading
import Queue
import multiprocessing
import traceback

from watchout.apps.events.models import Event

__all__ = ['BaseParser', ]


class BaseParser(object):
    """
    Base parser.
    """

    threads_num = multiprocessing.cpu_count()

    def __init__(self):

        self.queue = Queue.Queue()

    def parse(self):

        # fill queue
        for page in self.get_pages():
            self.queue.put(page)

        for i in xrange(self.threads_num):
            t = self.Worker(self.queue, self, "parse_page")  # creating thread
            t.start()  # starting thread

        self.queue.join()  # block execution before data existing

    def get_pages(self):
        """
        Return list of pages to parse.
        """

        return []

    def parse_page(self, url):
        """
        Parse page data.
        """

        pass

    def parser_item(self, obj):
        """
        Parse item.
        """

        pass

    def get_name(self, obj):
        """
        Get event name.
        """

        return u''

    def get_start(self, obj):
        """
        Get event start.
        """

        return u''

    def get_end(self, obj):
        """
        Get event end.
        """

        return u''

    def get_address(self, obj):
        """
        Get event address.
        """

        return u''

    def get_location(self, obj):
        """
        Get event location.
        """

        return u''

    def get_description(self, obj):
        """
        Get event description.
        """

        return u''

    def get_category(self, obj):
        """
        Get event category.
        """

        return u''

    def write_item(self, name, start, end, address, location, description, category):
        """
        Write parsed item to database.
        """

        Event.objects.create(name=name, start=start, end=end, address=address, location=location, description=description, category=category)

    class Worker(threading.Thread):
        """
        Multithreading.
        """

        def __init__(self, queue, __self__, runner, *args, **kwargs):
            threading.Thread.__init__(self)
            self.__queue = queue
            self.__self__ = __self__  # small hack to get access to main class methods
            self.runner = __self__.__getattribute__(runner)

        def run(self):
            while not self.__queue.empty():
                try:
                    item = self.__queue.get()  # wait data
                except Queue.Empty:
                    break  # data disappeared, stopping
                try:
                    self.runner(item)  # working
                except Exception:
                    self.__queue.task_done()  # job completed
                    continue

                self.__queue.task_done()  # job completed

            return
