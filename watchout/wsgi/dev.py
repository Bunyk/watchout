# watchout
# watchout/wsgi/dev.py

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "watchout.settings.dev")
application = get_wsgi_application()
