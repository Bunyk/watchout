#!/usr/bin/env python

# -*- coding: utf-8 -*-

# watchout
# manage.py

import os
import sys

from django.core.management import execute_from_command_line

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "watchout.settings.production")

    execute_from_command_line(sys.argv)
